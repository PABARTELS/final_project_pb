# final_project_pb

This project takes MERRA-2 Reanalysis data off of the western African coast, in the Atlantic 
Ocean on June 18, 2020. This code slices the desired MERRA-2 data and writes the thermodynamic
and extinction files used in the Fu-Liou-Gu (FLG) Radiative Transfer Model. The FLG model is 
ran outside of the notebook in FORTRAN, and the output text files are imported back into the
notebook. The heating rates are plotted, along with the surface temperatures and dust mixing
ratio.

There is a binder link available for this code, but running the code from the original repository
is recommended due to cartopy issues.
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.doit.wisc.edu%2FPABARTELS%2Ffinal_project_pb.git/HEAD)